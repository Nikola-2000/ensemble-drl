# ensemble-drl

## Required packages
Install the required packages specified in the requirements.txt run this project
    pip install -r requirements.txt

The Diambra platform is in a Docker container so Docker should be installed on the machine 
this project is running

## Running
The main.py script is the script for running the project. There are trained agents in the folders but you can
train your own agents.

run() - this method loads the agents which play the game and renders the gameplay
train_and_save() - this method trains new agents and saves them (Overwrites existing models)
evaluate() - this method gives the mean reward for every agent
tune() - this method tunes hyperparameters for a given algorithm and saves them in a JSON file

To run this you must use this command

    diambra run -r /path/to/rom python main.py